# Minimal ToDo

We'll build a command line Todo list app that reads CSVs, parses them into an Elixir map, and takes commands from user input. To be clear, this is a ridiculous project. There are numerous todo lists written in various JavaScript and mobile development frameworks but who has seen a command line todo list?