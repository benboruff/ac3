defmodule MinimalTodo do
  def start do
    filename = IO.gets("Name of .csv file to load:") |> String.trim
    read(filename)
# Ask user for filename
# Open file and read
# Parse file data
# Ask user for command
# (read todos, add todos, delete todos, load file, save file)
  end

  def read(filename) do
    case File.read(filename) do
    {:ok, body}  -> body
    {:error, reason} -> IO.puts ~s(Could not open file "#{filename}" \n)
                        IO.puts ~s("#{:file.format_error reason}")
                        start()

    end
  end

  def parse(body) do
    [header | items] = String.split(body, ~r{(\r\n|\r|\n)})
    titles = tl String.split(header, ",")
  end
end
